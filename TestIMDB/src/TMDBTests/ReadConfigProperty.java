package TMDBTests;

import java.awt.List;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

public class ReadConfigProperty {
	java.util.List<String> result;
	InputStream inputStream;
	String apikey,imdbid1,imdbid2,URL;
	java.util.List<String> myList = new ArrayList<String>();

	public java.util.List<String> getPropValues() throws IOException {
		
		try {
			Properties prop = new Properties();
			java.lang.String propFileName = "config.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 
			
			// get the properties values 
			apikey = prop.getProperty("apikey");
			imdbid1 = prop.getProperty("imdbid1");
			imdbid2 = prop.getProperty("imdbid2");
			URL = prop.getProperty("URL");
			myList.add(apikey);
			myList.add(imdbid1);
			myList.add(imdbid2);
			myList.add(URL);
			
    		//result  = apikey +imdbid1+ imdbid2 + URL;
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return myList;
	}

}

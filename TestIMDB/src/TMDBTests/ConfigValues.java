package TMDBTests;

import java.io.IOException;

public class ConfigValues {
	
	
		private String apikey;
		private String imdb_firstid;
		private String defaultURL;

		

		public String getApikey() {
			return readconfigvalues.get(0);
		}

		
		public ConfigValues() {
			ConfigValuesParsed();
			
		}
		
		java.util.List<String> readconfigvalues;
		ReadConfigProperty properties = new ReadConfigProperty();
		
		public void ConfigValuesParsed()
		{
			try {
				readconfigvalues = properties.getPropValues();
			} catch (IOException e) {
				// catch block
				e.printStackTrace();
			}
			
			
			
		}
		
		public String setApikey(String apikey) {
			return this.apikey= readconfigvalues.get(0);
		}

		/**
		 * @return the imdb_firstid
		 */
		public String getImdb_firstid() {
			return readconfigvalues.get(1);
		}

		public void setImdb_firstid(String imdb_firstid) {
			this.imdb_firstid = readconfigvalues.get(1);
		}
		
		/**
		 * @return the defaultURL
		 */
		public String getDefaultURL() {
			return readconfigvalues.get(3);
		}

		/**
		 * @param defaultURL the defaultURL to set
		 */
		public void setDefaultURL(String defaultURL) {
			this.defaultURL = readconfigvalues.get(3);
		}
	
	
}

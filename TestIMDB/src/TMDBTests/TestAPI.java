package TMDBTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParserFactory;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Test;



/**
 * @author AMAR MADUPU
 *@version 1.0
 * The class TestAPI has four Junit tests with each test serving a different functionality to test
 * All tests do require an apikey that can be generated from the website https://developers.themoviedb.org/3/getting-started/introduction
 * @param apikey is the key generated from configuration file
 * @param defaultURL points to the URL=https://api.themoviedb.org/3 and is also read from config file
 * 
 */
public class TestAPI {

	ReadConfigProperty properties = new ReadConfigProperty();
	java.util.List<String> readconfigvalues;
	ConfigValues config = new ConfigValues();
	String apikey , testResponse ;
	Client client = ClientBuilder.newClient();
	public interface RESTAPIEndpointTests{}
	public interface DiscoverEndpointTests extends RESTAPIEndpointTests{}
	
	

	/**
	 * These are left empty on purpose, they need to filled in with the test setup methods, currently not required as the web service is being called with a client and target assigned.
	 * @throws IOException
	 */
	@Before
	public void SetupForAPITests() throws IOException
	{

	}

	public TestAPI() {
	}


	/**
	 * This test is used as a smoke test where in a known movie and its attributes are returned from calling RESTAPI service with a validated apikey.
	 * @param apikey is the key generated from configuration file.
	 * @param defaultURL points to the URL=https://api.themoviedb.org/3 and is also read from config file.
	 * @throws IOException
	 */
	@Test
	public void SmokeTestAPI() throws IOException
	{

		WebTarget target = client.target(config.getDefaultURL() + "/movie/550?api_key="+ config.getApikey());
		testResponse  = target.request(MediaType.APPLICATION_JSON).get(String.class);

		String expectedResponseOverViewText = "A ticking-time-bomb insomniac";
		assertEquals(testResponse.contains(expectedResponseOverViewText), true);

	}

	/**
	 * This test is used to validate the "find" functionality used tohe find method makes it easy to search for objects in our database by an external id. For instance, an IMDB ID.
	 * @param apikey is the key generated from configuration file.
	 * @param defaultURL points to the URL=https://api.themoviedb.org/3 and is also read from config file.
	 * @param an IMDB ID is an external id corresponding to particular movie - "Fight Club" in this test and is read from config file.
	 * @throws IOException
	 */
	@Test
	public void FindByIDMethod() throws IOException
	{
		//  setting target for the client
		WebTarget target = client.target(config.getDefaultURL()+ "/find/" + config.getImdb_firstid() + "?api_key=" + 
				config.getApikey() + "&language=en-US&external_source=imdb_id");
		String testResponse = target.request(MediaType.APPLICATION_JSON).get(String.class);

		String expectedMovieTitle = "Fight Club";
		assertTrue(testResponse.contains(expectedMovieTitle));

	}

	/**
	 * The restapi related to the "Discover" functionality used to discover movies by different types of data like average rating, number of votes, genres and certifications.
	 * This test is used for getting the count of movies  released in theaters between the release dates specified.
	 * @param apikey is the key generated from configuration file.
	 * @param defaultURL points to the URL=https://api.themoviedb.org/3 and is also read from config file.
	 * @throws IOException
	 * 
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void DiscoverMoviesByReleaseDates() throws IOException
	{
		//  setting target for the client
		WebTarget target = client.target(config.getDefaultURL()+"/discover/movie?api_key="
				+ config.getApikey() + "&primary_release_date.gte=2017-09-17&primary_release_date.lte=2017-09-17");

		String testResponse = target.request(MediaType.APPLICATION_JSON).get(String.class);

		String expectedmovieCountBetweenReleaseDates = "23";
		assertTrue(testResponse.contains(expectedmovieCountBetweenReleaseDates));

	}
	

	/**
	 * The restapi related to the "Discover" functionality used to discover movies by different types of data like average rating, number of votes, genres and certifications.
	 * This test is created to validate multiple quieries, popularity filter along with released dates filter and results compared and validated per expected response.
	 * @param apikey is the key generated from configuration file.
	 * @param defaultURL points to the URL=https://api.themoviedb.org/3 and is also read from config file.
	 * @throws IOException
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void DiscoverMoviesByMostPopularity() throws IOException
	{
		//  setting target for the client
		WebTarget target = client.target(config.getDefaultURL()+"/discover/movie?api_key="
				+ config.getApikey() + "&sort_by=popularity.desc"+"&primary_release_date.gte=2017-09-17&primary_release_date.lte=2017-09-17");

		String testResponse = target.request(MediaType.APPLICATION_JSON).get(String.class);

		String expectedmovieCountBetweenReleaseDates = "2017-09-17";
		assertTrue(testResponse.contains(expectedmovieCountBetweenReleaseDates));

	}
	

	/**
	 * The restapi related to the "Discover" functionality used to discover movies by different types of data like average rating, number of votes, genres and certifications.
	 * This test is used to validate "TEST TO FAIL" scenario and expecting an Unauthorized 401 response.
	 * @param apikey is the key generated from configuration file, but is set to null on purpose here.
	 * @param defaultURL points to the URL=https://api.themoviedb.org/3 and is also read from config file.
	 * @throws IOException
	 * .
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void DiscoverMoviesTestToFailNullAPIkey() //throws IOException
	{
		String apikey =null,testResponse = null, responseCodeString = null;
		//  setting target for the client
		WebTarget target = client.target(config.getDefaultURL()+"/discover/movie?api_key="+
				apikey + "&sort_by=popularity.desc");

		try
		{
		
		
		 testResponse = target.request(MediaType.APPLICATION_JSON).get(String.class);
		}
		catch (Exception e)
		{
			responseCodeString=e.toString();
		}

		String expectedresponse = "HTTP 401 Unauthorized";
		assertTrue(responseCodeString.contains(expectedresponse));

	}
	

	/**
	 * The restapi related to the "Discover" functionality used to discover movies by different types of data like average rating, number of votes, genres and certifications.
	 * This test is used for validating the certification params used in tandom with sort by filters and cast id variables.
	 * @param apikey is the key generated from configuration file.
	 * @param defaultURL points to the URL=https://api.themoviedb.org/3 and is also read from config file.
	 * @throws IOException
	 * .
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void DiscoverMoviesByCertifications() throws IOException
	{
		//  setting target for the client
		WebTarget target = client.target(config.getDefaultURL()+"/discover/movie?api_key="
				+ config.getApikey() + "&certification_country=US&certification=R&sort_by=revenue.desc&with_cast=3896");

		String testResponse = target.request(MediaType.APPLICATION_JSON).get(String.class);

		String expectedmovieCountWithCertificationCastFilters = "31";
		assertTrue(testResponse.contains(expectedmovieCountWithCertificationCastFilters));

	}
	

	/**
	 * The restapi related to the "Discover" functionality used to discover movies by different types of data like average rating, number of votes, genres and certifications.
	 * This test is used for getting the count of TVS  with specific filters.
	 * @param apikey is the key generated from configuration file.
	 * @param defaultURL points to the URL=https://api.themoviedb.org/3 and is also read from config file.
	 * @throws IOException
	 *
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void DiscoverTVSList() throws IOException
	{
		//  setting target for the client
		WebTarget target = client.target(config.getDefaultURL()+"/discover/tv?api_key="
				+ config.getApikey() + "&language=en-US&sort_by=popularity.desc&page=1&timezone=America%2FNew_York&include_null_first_air_dates=true");

		String testResponse = target.request(MediaType.APPLICATION_JSON).get(String.class);

		String expectedmovieCountsForTVResultsList = "total_results";
		assertTrue(testResponse.contains(expectedmovieCountsForTVResultsList));

	}
	
	/**
	 * TODOs - No Code implemented.
	 * The purpose of the test is to validate correct response from end points that are targeted for TVLists that support different time zones and in other languages specific to those time zones.
	 * This would ensure the API is working for different timezones and their specific languages as well.
	 * @throws IOException.
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void DiscoverTVSListwithTimeZonesAndLanguages() throws IOException
	{
		//Build target API endpoint for TVLists that support different time zones and in other languages specific to those time zones
		// Validate the returned response
		//Make sure the assertions match the actual and expected response.
	}
	
	/**
	 * TODOs - No Code implemented.
	 * The purpose of the test is to validate correct response from end points that are targeted for Movies andTV Lists that are newly added.
	 * This would ensure the API is working for that scenario .
	 * @throws IOException
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void DiscoverMoviesandTVSListWhenNewOnesAdded() throws IOException
	{
		//Build target API endpoint for TVLists that support new movies or tvs discovered when they are added 
		// Validate the returned response
		//Make sure the assertions match the actual and expected response.
	}
	
	/**
	 * TODOs - No Code implemented.
	 * The purpose of the test is to validate correct response  when DB is down or during manintainence or downtime.
	 * The respone should be valid and ask user to try again as per the down time.
	 * @throws IOException
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void DiscoverMoviesandTVSListWhenDBLostConnection() throws IOException
	{
		//Build target API endpoint for TV Lists that support new movies or tvs discovered .
		// Validate the returned response that asks user something is wrong, please try again or message similar to the context specified.
		//Make sure the assertions match the actual and expected response.
	}
	
	/**
	 * TODOs - No Code implemented
	 * The purpose of the test is to validate correct response from end points that are targeted for Movies..
	 * This test would ensure the API is not working for incorrect date format and is informing the user of the same error.
	 * @throws IOException
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void DiscoverMoviesWhenReleaseDatesWrongFormat() throws IOException
	{
		//Build target API endpoint for TV Lists that support filters for released dates .
		// Validate the returned response that asks user date format is wrong, please try again or message similar to the context specified.
		//Make sure the assertions match the actual and expected response.
	}
	
	/**
	 * TODOs - No Code implemented.
	 * The purpose of the test is to validate the response for a service that is not yet created, this is not a happy path test and is expected to fail
	 * This test is used to validate "TEST TO FAIL" scenario and expecting an Unauthorized 501 response.
	 * @throws IOException
	 */
	
	@Test
	@Category(DiscoverEndpointTests.class)
	public void CreateDiscoverServiceNotImplemented() throws IOException
	{
		//Build target API endpoint for TV Movies with combination of /Find, /Search and /Discover, a service  that is not supported yet  
		// Validate the returned response similar to 501 or Invalid service: this service does not exist.
		//Make sure the assertions match the actual and expected response.
	}
	
	/**
	 * TODOs - No Code implemented.
	 * The purpose of the test is to validate correct response from end points that are targeted for Movies are saved to users preffered list.
	 * This would ensure the API is working  correctly and names added to the list is what is expected from the end user.
	 * @throws IOException
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void ValidateDiscoveredMoviesToSavedList() throws IOException
	{
		//Build target API endpoint to be able to add TV Movies list users preference list
		// Validate the list that correct names have been added.
		//Make sure the assertions match the actual and expected response.
	}
	
	/**
	 * TODOs - No Code implemented.
	 * The purpose of the test is to validate correct response from end points that are targeted for Movies that which were discovered but not watched or partially watched.
	 * This test would ensure the API is working as expected and have correct lists.
	 * @throws IOException.
	 */
	@Test
	@Category(DiscoverEndpointTests.class)
	public void RememberDiscoverMoviesNotification() throws IOException
	{
		//Build target API endpoint for Movies which were discovered but not watched or partially watched 
		// Create a list with list end points
		// Validate the returned response similar that the correct list has been added.
		//Make sure the assertions match the actual and expected response.
	}
	
	/**
	 * This test is used as to validate the response for authetication rest api call in order to create a temporary request token that can be used to validate a TMDb user login.
	 * @param apikey is the key generated from configuration file.
	 * @param defaultURL points to the URL=https://api.themoviedb.org/3 and is also read from config file.
	 * @throws IOException.
	 */
	@Test
	public void APIAuthenticateMethod() throws IOException
	{

		WebTarget target = client.target(config.getDefaultURL() +"/authentication/token/new?api_key="+ config.getApikey());
		testResponse = target.request(MediaType.APPLICATION_JSON).get(String.class);
		String expectedresponsecriteria = "success";
		assertEquals(testResponse.toString().contains(expectedresponsecriteria), true);

	}


}
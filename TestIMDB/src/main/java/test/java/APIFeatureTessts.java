package main.java.test.java;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

public class APIFeatureTessts {
  @Given("I have generated an API Key")
  public void given() throws Throwable {
	  System.out.println("this is the given");
  }

  @When("I call an endpoint with the key")
  public void when() throws Throwable {
	  System.out.println("this is the when");
  }

  @Then("my response is json")
  public void then() throws Throwable {
	  System.out.println("this is then");
  }

}

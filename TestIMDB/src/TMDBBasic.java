import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class TMDBBasic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// 1. Creating the client
		Client client = ClientBuilder.newClient();
		
		// 2. setting target for the client
		WebTarget target = client.target("https://api.themoviedb.org/3/movie/550?api_key=38c8b77cc0fb5ba9f26b786f8206a777");
		System.out.println(
				target.request(MediaType.APPLICATION_JSON).get(String.class));
		
	}

}

